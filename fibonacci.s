####
# CST 307 Cache Benchmark
# Instructor: Jeff Griffith
# Author: Shaun, Moses, Chandler
# Description
# Fibonacci Sequence: The Fibonacci sequence is a series of numbers where a number is
# found by adding up the two numbers before it. Starting with 0 and 1, the sequence goes
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, and so forth. Written as a rule, the expression is
# xn = x(n-1) + x(n-2).
# 
# Bitbuck URL: https://shaun86wang@bitbucket.org/shaun86wang/cst-307.git
####

# 
# Data Declarations
#

.data
prompt: .ascii  "\n\nFibonacci Program\n\n"
		    .asciiz "Enter N value: "

results: .asciiz "\nFibonacci sequence with N numbers  "

again:	.asciiz "\nDo you want to try again? [y/n] "

thankYou:	.asciiz "\n\nThank you "

seperator:	.asciiz "  "

answer:		.word 0 # nth number in fibo seq

retry = 0x79

# 
# Text/code section
#

.text
.globl main
main:

runAgain:
		li	$v0, 4 				# ask system to print string
		la	$a0, prompt			# pass in the prompt string
		syscall					# print string
		
		li $v0, 5 				# read the number of number in the sequence user wants to print (as integer)
		syscall					# retrieve user input

		move $s0, $v0			# save user input to $s0

		li	$v0, 4 				# ask system to print string
		la	$a0, results		# pass in the result string
		syscall					# print string

		li $s1, 0				# initilize the starting position (first fibonacci number)

loop:	
		add	$a0, $s1, $0		# parameter integer to fibo function
		blt $a0, 2, resultOne	# if the parameter is less than 2, directly assign result to 1

		jal	fibo				# call the fibonacci function

saveAnswer:
		sw	$v0, answer			# store output of fibo function
		
		li	$v0, 1				# ask system to print integer
		lw	$a0, answer			# pass in the fibonacci number as argument
		syscall					# print integer

		li $v0, 4				# ask system to print string
		la $a0, seperator		# pass in seperator
		syscall					# print string

next:
		addi $s1, $s1, 1		# increment the position of number
		blt $s1, $s0, loop		# keep printing numbers until reach user input
		
		li	$v0, 4 				# ask system to print string
		la	$a0, again			# pass in the confirmation string
		syscall					# print string
	
		li $v0, 12 				# ask system to read character
		syscall					# read character

		beq	$v0, retry, runAgain	# if user is done then exit else get another number

		li	$v0, 4 				# ask system to print string
		la	$a0, thankYou		# pass in thank you string
		syscall					# print string


		li	$v0, 10 			# call code for terminate
		syscall 				# system call

resultOne:
		li $v0, 1				# set the result to 1
		j saveAnswer			# jump back to save answer

.end main



#
# Name:	fibo
#
# Description:
#	Given an integer n, will return the nth number in the fibonacci sequence
#
# Recursive definition:
# 	= if n<2, then fibo(n) = 1
# 	= else fibo(n) = fibo(n-1) + fibo(n-2)
# 
# Arguments
#	$a0 the number for n
#
# Returns
# 	$v0 the fibonacci result
#
# Call frame:
#	sp-8 $ra
#	sp-4 $s0
#

.globl fibo
.ent fibo
fibo:
	subu	$sp, $sp, 12			# adjust stack so we can store 3 words = 12 bytes
	sw		$ra,  ($sp)				# store $ra return address register
	sw		$s0, 4($sp)				# store $s0 as the input N
	sw		$s1, 8($sp)

	move	$s0, $a0				# save input
	li		$s1, 0					# initilize result

	li		$v0, 1 					# check base case
	blt		$s0, 2, fiboDone

	sub		$a0, $s0, 1				# pass in arg for fibo(n-1)

	li  $t0, 0						# mark that the fibonacci function belongs to the left side of addition
	blt $a0, 2, resultOneFibo		# if the parameter is less than 2, directly assign result to 1

	jal fibo						# call fibo(n-1)

saveAnswer1:
	add $s1, $s1, $v0 				# result += fibo(n-1)

	sub		$a0, $s0, 2				# pass in arg for fibo(n-2)

	li $t0, 1						# mark that the fibonacci function belongs to the right side of addition
	blt $a0, 2, resultOneFibo		# if the parameter is less than 2, directly assign result to 1

	jal fibo						# call fibo(n-2)

saveAnswer2:
	add $s1, $s1, $v0 				# n + fibo(n-1)

	move $v0, $s1					# return result

fiboDone:
	lw		$ra,  ($sp)				# restore stored $ra return address
	lw		$s0, 4($sp)				# restore stored $s0
	lw		$s1, 8($sp)				# restore stored $s1
	addu	$sp, $sp, 12			# adjust stack back to called size
	
	jr $ra

resultOneFibo:
		li $v0, 1				# set the result to 1
		beq $t0, 1, saveAnswer2	# if the marker is 1, jump back to the next line after the second fibo function to save answer
		j saveAnswer1			# otherwise, jump back to the next line after the first fibo function to save answer

	
.end fibo
